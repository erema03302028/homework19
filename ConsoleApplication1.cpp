#include <iostream>

using namespace std;

class Animal
{
public:
    virtual void voice()
        {
            cout << "Text \n";
        }
};

class Cat : public Animal
{
public:
    void voice() override
    {
        cout << "Meow \n";
    }
};

class Dog : public Animal
{
public:
    void voice() override
    {
        cout << "Woof \n";
    }
};

class Spider : public Animal
{
public:
    void voice() override
    {
        cout << "Phh \n";
    }
};

int main()
{
    Animal* array[3];
    array[0] = new Dog;
    array[1] = new Cat;
    array[2] = new Spider;

    for (int i = 0; i < 3; i++)
    {
        array[i]->voice();
    }

    return 0;
}